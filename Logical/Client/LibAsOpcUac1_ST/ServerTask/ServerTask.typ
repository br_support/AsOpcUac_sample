(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ServerTask
 * File: ServerTask.typ
 * Created: August 07, 2014
 ********************************************************************
 * Local data types of program ServerTask
 ********************************************************************)

TYPE
	EnumTyp1 : 
		(
		Red := 0,
		Green := 1,
		Blue := 2
		);
END_TYPE
